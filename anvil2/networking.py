import sys
import socket
import select
import time
import logging
import queue


# Define max buffer sizes
UDP_BUFFER_SIZE = 65536
TCP_BUFFER_SIZE = 1000000

# Define max connections allowed
TCP_MAX_CONNECTIONS = 1


class _NetworkingNode(object):
    """ Class handles the lower level handling of getting messages
    to and from sockets
    """

    def _createListeningSocket(self):

        # Create UDP socket
        try:
            logging.debug("Creating {0} signaling listening socket at {1}:{2}".format(
                self._signaling_transport, self._local_ip, self._local_port))
            listening_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            logging.debug("Socket {0} created".format(listening_socket))
        except (socket.error) as msg:
            raise Exception("Failed to create socket. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

        # Set UDP socket options
        try:
            logging.debug("Setting socket {0} to non blocking".format(listening_socket))
            listening_socket.setblocking(0)
            logging.debug("Setting socket options")
            listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        except (socket.error) as msg:
            raise Exception("Failed to set socket options. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

        # Bind the UDP socket
        try:
            logging.debug("Binding the socket to port {0}:{1}".format(self._local_ip, self._local_port))
            listening_socket.bind((self._local_ip, self._local_port))
        except (socket.error) as msg:
            raise Exception("Failed to bind socket. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

        logging.debug("listening socket = {0}".format(listening_socket))
        return listening_socket


    def send(self, socket, data, ip, port):
        return socket.sendto(bytes(data), (ip, port))


    def recv(self, socket, timeout=1):
        socket.settimeout(timeout)
        data = socket.recv(UDP_BUFFER_SIZE)
        return data, socket.getpeername()[:2]

    def stop(self, socket):
        socket.close()



class _Endpoint(_NetworkingNode):
    """Class handles the event queue of what an endpoint is sending and expecting to receive

    Also handles scenario file parsing and translating those into events"""

    def __init__(self):
        self._event_queue = queue.Queue()


    def get_next_event(self):
        "Pop the next event off the queue and process it"
        pass

    def add_event(self, type):
        "Add an event to the queue (send, recv, pause, parse_message, etc)"
        pass

    def parse_scenario_file(self, scenario):
        "Processes an XML scenario file for events. Creates a scenario of sending and receiving messages"
        pass

    def stop(self, socket):
        super(_NetworkingNode, self).stop(socket)




class SIPEndpoint(_Endpoint):
    """Class handles SIP specific functionality"""

    def __init__(self, name, local_ip, local_port, remote_ip='127.0.0.1', remote_port=5060):
        self._signaling_handler = 'SIP'
        self._signaling_transport = 'UDP'
        self._media_type = 'RTP'

        self._name = str(name)
        self._local_ip = str(local_ip)
        self._local_port = int(local_port)
        self._remote_ip = str(remote_ip)
        self._remote_port = int(remote_port)
        self._default_recv_timeout = 1

        self._sockets = []
        self._signaling_socket = self._createListeningSocket()
        self._sockets.append(self._signaling_socket)


    def send_message(self, message):
        self.send(self._signaling_socket, str(message).encode('utf-8'), self._remote_ip, self._remote_port)

    def send_sip_message(self, message):
        pass

    def recv_message(self, message, timeout=None):
        try:
            recv_message, ip, port = self.recv(self._signaling_socket, timeout=self._default_recv_timeout)
        except:
            raise Exception("Failed to receive a message")

        if message == recv_message:
            logging.info("Received expected message of {0} from {1}:{2}".format(recv_message, ip, port))
        else:
            raise Exception("Expected message {0} but received message {1} from {2}:{3}"
                            .format(message, recv_message, ip, port))


    def stop(self):
        logging.info("Stopping {}".format(self._name))
        self._stop_listening()

    def _stop_listening(self):
        for socket in self._sockets:
            super(_Endpoint, self).stop(socket)



def main():
    print("testing")
    test = SIPEndpoint('uac', '127.0.0.1', 5060, '127.0.0.1', 5061)
    test.send_message('hi world')
    test.stop()


if __name__ == '__main__':
    main()
class _NamedCache(object):

    def __init__(self, basename, miss_error):
        self._basename = basename
        self._name_counter = 0
        self._entries = 0
        self._cache = {}
        self._current = None
        self._miss_error = miss_error

    def add(self, value, name=None):
        name = name or self._next_name()
        self._cache[name] = value
        value.name = name
        self._current = name
        self._entries += 1

    def _next_name(self):
        self._counter += 1
        return self._basename + str(self._counter)

    def get_with_name(self, name=None):
        if not name:
            name = self._current
            if not name:
                raise AssertionError(self._miss_error)
            logger.debug("Choosing %s by default" % self._current)
        return self._cache[name], name

    def get(self, name=None):
        return self.get_with_name(name)[0]

    def __iter__(self):
        return self._cache.values()

    def __len__(self):
        return self._entries

    def set_current(self, name):
        if name in self._cache:
            self._current = name
        else:
            raise KeyError("Name %s unknown." % name)

    def get_keys(self):
        return self._cache.keys()
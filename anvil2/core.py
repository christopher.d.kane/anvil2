from threading import Thread
import logging
import sys
import time
#from networking import SIPEndpoint
from .scenario import SipScenario
from .structures import _NamedCache


class anvil2Core(object):

    def __init__(self):
        self._create_internal_lists()
        self._setup_environment()


    def _create_internal_lists(self):
        self._endpoints = _NamedCache('endpoints', "No endpoints defined")


    def _setup_environment(self):
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(message)s')


    def print_version_in_core(self):
        print("Version is 1.0core")


    def reset_endpoints(self):
        pass


    def get_sip_endpoint(self, name):
        return self._endpoints.get_with_name(name)


    def get_sip_endpoints_count(self):
        return len(self._endpoints)


    def create_sip_endpoint(self, name, ip, port, remote_host='127.0.0.1', remote_port=5060):
        logging.info("Creating sip scenario {0} with local addr {1}:{2} and remote addr {3}:{4}".format(name, ip, port, remote_host, remote_port))
        endpoint = SipScenario(name, ip, port, remote_host, remote_port)
        self._endpoints.add(endpoint, name)


    def start_sip_endpoint(self, name):
        endpoint = self._endpoints.get_with_name(name)[0]
        if not endpoint:
            raise Exception("SIP endpoint with name {} does not exist. Failure to start SIP endpoint".format(name))
        else:
            endpoint.start()

    def stop_sip_endpoint(self, name):
        endpoint = self._endpoints.get_with_name(name)[0]
        if not endpoint:
            raise Exception("SIP endpoint with name {} does not exist. Failure to stop SIP endpoint".format(name))
        else:
            endpoint.stop()


    def send_message(self, name, message):
        endpoint = self._endpoints.get_with_name(name)[0]
        if endpoint:
            endpoint.send(message)
        else:
            raise Exception("SIP endpoint with name {} does not exist. Failure to send message".format(name))


    def receive_message(self, name, message):
        endpoint = self._endpoints.get_with_name(name)[0]
        if endpoint:
            endpoint.recv(message)
        else:
            raise Exception("SIP Endpoint with name {} does not exist. Failure to receive SIP message".format(name))


    def verify_received_message(self, name, expected_message, timeout=1):
        endpoint = self._endpoints.get_with_name(name)[0]
        if endpoint:
            endpoint.verify_received_sip_packet(expected_message, timeout)
        else:
            raise Exception("SIP Endpoint with name {} does not exist. Failure to verify received "
                            "SIP message".format(name))


    def stop_all_endpoints(self):
        endpoints = self._endpoints.get_keys()
        for endpoint in endpoints:
            self._endpoints.get_with_name(endpoint)[0].stop()


    def run_scenario(self):
        pass


def main():

    newcore = anvil2Core()
    newcore.print_version_in_core()
    newcore.create_sip_endpoint('uac', "127.0.0.1", 5060, "127.0.0.1", 5061)
    newcore.create_sip_endpoint('uas', "127.0.0.1", 5061, "127.0.0.1", 5060)
    newcore.start_sip_endpoint('uac')
    print("number of endpoints is {}".format(newcore.get_sip_endpoints_count()))
    print("details of uac endpoint is {}".format(newcore.get_sip_endpoint('uac')))
    test_endpoint, test_endpoint_name = newcore.get_sip_endpoint('uac')
    print("type of endpoint is {}".format(type(test_endpoint)))
    test_endpoint.send("test")
    time.sleep(2)
    newcore.stop_all_endpoints()

if __name__ == '__main__':
    main()
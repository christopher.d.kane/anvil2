import sys
import socket
import logging
import threading
import select
import queue
import time

# Define universal socket timeout
SOCKET_TIMEOUT = 1

# Define max buffer sizes
UDP_BUFFER_SIZE = 65536
TCP_BUFFER_SIZE = 1000000

# Define max connections allowed
TCP_MAX_CONNECTIONS = 1


class _Scenario(threading.Thread):
    """Base class for creating anvil2 scenarios, which handle different events such as sending and receiving network
    packets"""

    def __init__(self, name, local_ip, local_port):
        super(_Scenario, self).__init__()
        self._name = str(name)
        self._local_ip = str(local_ip)
        self._local_port = int(local_port)

        self._alive = threading.Event()
        self._alive.set()
        self._scenario_started = False

        self._events = queue.Queue()
        self._sent_events = []
        self._recv_events = []


    def _createSocket(self, family=socket.SOCK_DGRAM):
        if family == socket.SOCK_DGRAM:
            # Create UDP socket
            try:
                logging.debug("Creating {0} signaling listening socket at {1}:{2}".format(
                    'UDP', self._local_ip, self._local_port))
                listening_socket = socket.socket(socket.AF_INET, family)
                logging.debug("Socket {0} created".format(listening_socket))
            except (socket.error) as msg:
                raise Exception("Failed to create socket. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

            # Set UDP socket options
            try:
                logging.debug("Setting socket {0} to non blocking".format(listening_socket))
                listening_socket.setblocking(0)
                logging.debug("Setting socket options")
                listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            except (socket.error) as msg:
                raise Exception("Failed to set socket options. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

            # Bind the UDP socket
            try:
                logging.debug("Binding the socket to port {0}:{1}".format(self._local_ip, self._local_port))
                listening_socket.bind((self._local_ip, self._local_port))
            except (socket.error) as msg:
                raise Exception("Failed to bind socket. Error {0}, Reason {1}".format(str(msg[0]), str(msg[1])))

            logging.debug("listening socket = {0}".format(listening_socket))
            return listening_socket
        else:
            raise Exception("Unsupported socket type {}. Unable to create a socket".format(family))


    def run(self):
        """Override the threading run. When a scenario is running, it is processing events from its queue"""
        while self._alive.isSet():
            try:
                event = self._events.get(True, 0.1)
                logging.debug("Processing event {}".format(event))
                for event_type, event_value in event.items():
                    logging.debug("Event type {}".format(event_type))
                    logging.debug("Event value {}".format(event_value))

                if event_type == 'send_event':
                    self._handle_send_event(event_value)
                elif event_type == 'recv_event':
                    self._handle_receive_event(event_value)
                elif event_type == 'verify_recv_event':
                    self._handle_verify_receive_event(event_value)
                elif event_type == 'pause_event':
                    self._handle_pause_event(event_value)
                else:
                    raise Exception("Error: Unknown event type {}".format(event))
            except queue.Empty as e:
                continue

    def start(self):
        logging.info("Starting {} now".format(self._name))
        self._scenario_started = True
        threading.Thread.start(self)


    def join(self, timeout=None):
        self._alive.clear()
        threading.Thread.join(self, timeout)


    def _handle_send_event(self, event):
        logging.info("scenario {0} received send event {1}".format(self._name, event))
        bytes_sent = self._send(event['socket'], event['message'], self._remote_ip, self._remote_port)

        if bytes_sent:
            event['bytes_sent'] = bytes_sent
            self._sent_events.append(event)
        else:
            raise Exception("Error processing send event in {}. Unable to send packets".format(self._name))


    def _handle_receive_event(self, event):
        logging.info("scenario {0} received receive event {1}".format(self._name, event))
        bytes_recv = self._recv(event['socket'], event['timeout'])
        logging.debug("Received bytes in _handle_receive_event of {}".format(bytes_recv))

        if bytes_recv:
            event['data_received'] = bytes_recv
            #event['data_received_from'] = (ip, port)
            self._recv_events.append(event)
            logging.debug("Recv events now {}".format(self._recv_events))
        else:
            raise Exception("Received no data within {}s".format(event['timeout']))


    def _handle_verify_receive_event(self, event):
        last_event = self._recv_events[-1]
        bytes_received = last_event['data_received'].decode('ascii')
        bytes_expected = last_event['expected_message']
        logging.debug("bytes received {}".format(bytes_received))
        logging.debug("bytes expected {}".format(bytes_expected))

        if bytes_received == bytes_expected:
            logging.info("Received expected message {}".format(bytes_expected))
        else:
            raise Exception("Failure verifying message. Expected {0} but received {1}".format(bytes_expected, bytes_received))


    def _handle_pause_event(self, event):
        logging.info("scenario {0} received pause event {1}".format(self._name, event))


    def _send(self, send_socket, message, ip, port):
        logging.info("sending {0} to {1}:{2}".format(message, ip, port))
        return send_socket.sendto(bytes(message), (ip, port))


    def _recv(self, recv_socket, timeout=SOCKET_TIMEOUT):
        logging.info("Receiving data on socket {}".format(recv_socket))

        data = ""
        data_from = ()
        readable, writeable, errors = select.select([recv_socket], [], [], timeout)

        for s in readable:
            try:
                data = s.recv(UDP_BUFFER_SIZE)
                logging.debug("Received data {0} on socket {1} from {2}".format(data, s, data_from))
            except:
                logging.debug("No data received")

        logging.debug("Returning from _recv")
        return data


    def get_scenario_name(self):
        return str(self._name)



class SipScenario(_Scenario):
    """SIP Scenario class"""

    default_recv_timeout = 10

    def __init__(self, name, local_ip, local_port, remote_ip='127.0.0.1', remote_port=5060):

        self._remote_ip = str(remote_ip)
        self._remote_port = int(remote_port)
        _Scenario.__init__(self, name, local_ip, local_port)
        self._sent_packets = queue.Queue()
        self._recv_packets = queue.Queue()
        self._signaling_socket = self._createSocket()

    def _print_scenario_info(self):
        logging.info("scenario name {}".format(self._name))
        logging.info("scenario local address {0}:{1}".format(self._local_ip, self._local_port))
        logging.info("scenario remote address {0}:{1}".format(self._remote_ip, self._remote_port))


    def send(self, message):
        if not self._remote_port or not self._remote_ip:
            raise Exception("SipScenario must have a remote IP and port to send packets to")
        else:
            event = {'send_event': {'socket': self._signaling_socket,
                                             'message': str(message).encode('ascii')}}
            self._events.put(event)
            logging.debug("put send event on the event queue")


    def recv(self, message, timeout=default_recv_timeout):
        if not self._remote_port or not self._remote_ip:
            raise Exception("SipScenario must have a remote IP and port to receive packets from")
        else:
            self._events.put({'recv_event': {'socket': self._signaling_socket,
                                             'expected_message': message, 'timeout': timeout}})
            self.verify_received_sip_packet(message, timeout)


    def verify_received_sip_packet(self, packet, timeout):
        self._events.put({'verify_recv_event': {'socket': self._signaling_socket,
                                             'expected_message': str.encode(packet), 'timeout': timeout}})


    def stop(self):
        logging.info("Stopping {} now".format(self._name))
        if self._scenario_started:
            _Scenario.join(self)


def main():
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)s %(levelname)s:%(threadName)s:%(message)s')
    test1 = SipScenario("test1", '127.0.0.1', 5060, '127.0.0.1', 5061)
    test2 = SipScenario("test2", '127.0.0.1', 5061, '127.0.0.1', 5060)

    test1._print_scenario_info()
    test2._print_scenario_info()

    test1.start()
    test2.start()
    test2.recv('hi world how are we today')
    time.sleep(1)
    test1.send('hi world how are we today')
    time.sleep(10)
    test1.stop()
    test2.stop()


if __name__ == '__main__':
    main()
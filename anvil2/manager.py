import sys
from .core import anvil2Core

class manager(anvil2Core):
    """SIP packet generation library for robot framework"""

    ROBOT_LIBRARY_VERSION = 1.0

    def get_version(self):
        return self.ROBOT_LIBRARY_VERSION


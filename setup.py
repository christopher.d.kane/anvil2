import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "anvil2",
    version = "0.0.1",
    author = "Christopher Kane",
    author_email = "christopher.d.kane@gmail.com",
    description = ("A functional test tool designed for SBC testing. \
                    Used by Oracle Acme QA"),
    license = "BSD",
    keywords = "anvil, anvil2",
    packages=['anvil2'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Topic :: Testing",
        "License :: OSI Approved :: BSD License",
    ],
    include_package_data=True,
    install_requires=['robotframework'],
    zip_safe=False
)

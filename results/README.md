This directory stores the output of the Robot tests being run. By default 3 files will exist:

log.html

output.xml

report.html

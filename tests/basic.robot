*** Variables ***
${LIBRARIES}    anvil2
${UAC_NAME}     uac
${UAS_NAME}     uas
${UAC_IP}       127.0.0.1
${UAS_IP}       127.0.0.1
${UAC_PORT}     5060
${UAS_PORT}     5061
${CLEANUP}      test case cleanup

*** Settings ***
Library     anvil2.manager

*** Test Cases ***
Get anvil2 version
    [documentation]  This test case ensures we are using robot library version 1.0
    ${result} =   get version
    should be equal as numbers  ${result}   1.0

Create basic SIP endpoint
    [documentation]  Tests the constructor works for the sipEndpoint class
    ${endpoint} =   create sip endpoint     uac     ${UAC_IP}  ${UAC_PORT}    ${UAS_IP}  ${UAS_PORT}
    ${number_of_endpoints} =    get sip endpoints count
    should be equal as numbers  ${number_of_endpoints}  1


UAC sends message to UAS
    [documentation]  Tests the constructor works for the sipEndpoint class
    create sip endpoint     ${UAC_NAME}     ${UAC_IP}  ${UAC_PORT}    ${UAS_IP}  ${UAS_PORT}
    create sip endpoint     ${UAS_NAME}     ${UAS_IP}  ${UAS_PORT}    ${UAC_IP}  ${UAC_PORT}
    start sip endpoint      ${UAS_NAME}
    start sip endpoint      ${UAC_NAME}
    ${number_of_endpoints} =    get sip endpoints count
    should be equal as numbers  ${number_of_endpoints}  2
    send basic message  ${UAC_NAME}     ${UAS_NAME}     hello there
    send basic message  ${UAS_NAME}     ${UAC_NAME}     hello there right back to you
    [Teardown]  ${CLEANUP}

UAS sends message to UAC
    [documentation]  Tests the constructor works for the sipEndpoint class
    create sip endpoint     ${UAS_NAME}     ${UAS_IP}  ${UAS_PORT}    ${UAC_IP}  ${UAC_PORT}
    create sip endpoint     ${UAC_NAME}     ${UAC_IP}  ${UAC_PORT}    ${UAS_IP}  ${UAS_PORT}
    start sip endpoint      ${UAS_NAME}
    start sip endpoint      ${UAC_NAME}
    ${number_of_endpoints} =    get sip endpoints count
    should be equal as numbers  ${number_of_endpoints}  2
    send basic message  ${UAS_NAME}     ${UAC_NAME}     hello there
    send basic message  ${UAC_NAME}     ${UAS_NAME}     hello there right back to you
    [Teardown]  ${CLEANUP}

*** Keywords ***
Send basic message
    [Arguments]     ${sender}   ${receiver}     ${message}
    send message    ${sender}   ${message}
    receive message  ${receiver}    ${message}
    verify received message  ${receiver}    ${message}
    log  endpoint ${receiver} received expected message ${message}

test case cleanup
    stop all endpoints



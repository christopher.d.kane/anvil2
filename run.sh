#!/usr/bin/env bash

set -e

path="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

: ${BUILD_ARGS:=""}
: ${BUILD_DIR:="$path"}
: ${BUILD_NAME:=${PWD##*/}}
: ${RUN_ARGS:=''}

### build and run tests ########################################################

docker build $BUILD_ARGS --tag "anvil2:$BUILD_NAME" "$BUILD_DIR"

docker run --rm -ti -e HOST_UID=$(id -u) -e HOST_GID=$(id -g) $RUN_ARGS \
  -v "$path/tests":/home/robot/tests \
  -v "$path/results":/home/robot/results \
  "anvil2:$BUILD_NAME" "${@:-tests}"
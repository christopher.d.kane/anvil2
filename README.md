# ANVIL2
Home of Anvil2, a functional test tool designed for SBC testing using SIP. Used by Oracle ACME Engineering

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development.

### Prerequisites
Anvil2 uses the following:

[Python3+](https://www.python.org/downloads/)

[Robot Framework](http://www.robotframework.org)

[Docker](https://www.docker.com/) + Internet connection to pull down base Docker image


### Running Anvil2
Clone the git repository
```
$ git clone https://gitlab.com/christopher.d.kane/anvil2.git
```

Run the tests using Robot and a Docker container
```
$ sudo chmod +x run.sh
$ ./run.sh
```

Results will be viewable in the results directory
```
$ cd results
```

To run with Robot debugging enabled
```
$ ./run.sh --loglevel TRACE tests/
```

## Authors
Christopher Kane - christopher.d.kane@gmail.com 




